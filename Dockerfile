FROM alpine:3.4
RUN apk --update upgrade \
    && apk --no-cache --no-progress add ca-certificates \
    && rm -rf /var/cache/apk/*
RUN apk add --no-cache curl
COPY ./conf/traefik /usr/local/bin/
COPY ./conf/traefik.toml /etc/traefik/
COPY ./conf/entrypoint.sh /
EXPOSE 80
RUN chmod +x /usr/local/bin/traefik
RUN chmod +x /entrypoint.sh
RUN chmod 777 /etc/traefik/traefik.toml
ENTRYPOINT ["/entrypoint.sh"]
CMD ["--help"]

# Metadata
LABEL org.label-schema.vendor="Containous" \
      org.label-schema.url="https://traefik.io" \
      org.label-schema.name="Traefik" \
      org.label-schema.description="A modern reverse-proxy" \    
      org.label-schema.version="v1.3.5" \
      org.label-schema.docker.schema-version="1.0"