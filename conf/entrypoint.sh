#!/bin/sh
set -e

if [ $1 == "test" ]; then
	echo "Starting Traefik"
	traefik --web --docker --docker.domain=docker.localhost --logLevel=DEBUG &
	sleep 1
	while ! grep -m1 'Server configuration reloaded' <  /var/log/traefik.log; do
		sleep 1
	done
	echo "Traefik Start"
	response=$(curl --write-out %{http_code} --silent --output 127.0.0.1 127.0.0.1:8080)
	echo $response
	boncode="200"
	if [ $response == "200" ] || [ $response == "302" ]; then
		echo "Traefik Responding on port 8080 Good job!";
		exit 0;
	else
		echo "Traefik not Responding on port 8080 Failed";
		exit 1;
	fi
else
	traefik --web --docker --docker.domain=docker.localhost --logLevel=DEBUG 
fi 



exec "$@"